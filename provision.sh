#!/bin/bash 

oc new-project etcd-backup

oc create -f etcd-backup-rbac.yaml

oc create -f etcd-backup-config.yaml

oc create -f etcd-backup-cronjob.yaml

backupName=$(date "+etcd-backup-manual-%F-%H-%M-%S")
oc create job --from=cronjob/etcd-backup ${backupName}

oc logs -l job-name=${backupName}